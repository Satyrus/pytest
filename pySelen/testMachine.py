from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import json
import time
import os


class TestMachine:
    def __init__(self, driver):
        self.driver = driver
        self.rootdir = os.path.dirname(__file__) + '\\..'
        # self.rootdir = r'C:\SoursetreeProjects\ProgTestSite_01\progTSwithJSON_01'
        self.pages = None
        self.currentPage = None
        self.loadPageObj()

    def get(self, link):
        self.driver.get(link)

    def loadPage(self):
        '''
        Переход по ссылке
        '''
        self.get(self.currentPage['link'])

    def loadPageObj(self):
        '''
        Загрузка JSON файла и добвление в массив
        '''
        self.pages = []
        fileName = 'mainPage.json'
        f = self.rootdir + '\\pageObjects\\' + fileName
        file = open(f, 'r', encoding='utf-8')
        page = json.load(file)
        self.pages.append(page)

    def setCurrPage(self, pageName):
        '''
        Функция по назначению текущей страницы
        :param pageName:
        '''
        for item in self.pages:
            if item['name'].lower() == pageName.lower():
                self.currentPage = item
                break

    def clicktButton(self, elemName):
        '''
        Функция для использования (Click) выбранного элемента по XPATH
        :param elemName:
        '''
        time.sleep(1)
        elemType = 'button'

        for item in self.currentPage["elements"]:
            if item["name"].lower() == elemName.lower() and item['type'].lower() == elemType.lower():
                elemBy = item["by"]
                elemPath = item["valueBy"]
                break

        wait = WebDriverWait(self.driver, 10)
        elem = wait.until(ec.element_to_be_clickable(('xpath', elemPath)))
        elem.click()


    def sendKeysToField(self, sendKey, field):
        time.sleep(1)
        elemType = 'field'

        for item in self.currentPage["elements"]:
            if item["name"].lower() == field.lower() and item['type'].lower() == elemType.lower():
                elemBy = item["by"]
                elemPath = item["valueBy"]


        elem = self.driver.find_element(By.XPATH, elemPath)
        elem.click()
        elem.send_keys(sendKey)
        time.sleep(1)


    def selectOption(self, catName, itemMenu):
        time.sleep(1)
        elemType = 'selectList'

        for item in self.currentPage["elements"]:
            if item["name"].lower() == catName.lower() and item['type'].lower() == elemType.lower():
                elemBy = item["by"]
                elemPath = item["valueBy"]


        path = elemPath + '//*[contains(text(),"' + itemMenu + '")]'
        wait = WebDriverWait(self.driver, 10)
        elem = wait.until(ec.element_to_be_clickable(('xpath', path)))
        elem.click()

    def selectItemOption(self, popupList, popupItem):
        '''
        Поиск в списке и использование(click) текстового элемента по XPATH
        :param catName: Название списка
        :param itemMenu: Элемент списка
        '''
        time.sleep(1)
        elemType = 'selectList'

        for item in self.currentPage["elements"]:
            if item["name"].lower() == popupList.lower() and item['type'].lower() == elemType.lower():
                elemBy = item["by"]
                elemPath = item["valueBy"]


        path = elemPath
        # !!! ожидание для поиска нескольких элементов, ec.elementS_to_be_clickable ?
        elems = self.driver.find_elements('xpath', path)
        for elem in elems:
            if elem.text == popupItem:
                elem.click()
                time.sleep(1)
                break

    def isTextOnPage(self, text):
        time.sleep(1)
        try:
            element = self.driver.find_element(By.XPATH, '//*[contains(text(),"'+text+'")]')
            return True
        except:
            return False


    def loadItemPage(self, itemName):
        '''
        Поиск и использование(click) элемента элемента на странице по XPATH
        :param itemName: Название элемента
        '''
        time.sleep(2)
        path = '//*[text() = "' + itemName + '" or @title = "' + itemName + '"]'
        wait = WebDriverWait(self.driver, 10)
        elem = wait.until(ec.element_to_be_clickable((By.XPATH, path)))
        elem.click()

    def closeWin(self):
        '''
        Функция закрытия тестируемого окна
        '''
        time.sleep(2)
        self.driver.quit()

import pytest
from selenium import webdriver
from pySelen.testMachine import TestMachine as TM


@pytest.fixture(scope='session')
def browser():
    dr = webdriver.Firefox(executable_path=r'C:\SoursetreeProjects\SeleniumProg\ProgSelen\webdriver\geckodriver.exe')
    tm = TM(dr)
    yield tm

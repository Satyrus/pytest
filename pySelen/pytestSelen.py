from pytest_bdd import given, when, then, parsers
from pySelen.testMachine import TestMachine  as TM



@when(parsers.parse('Работаю с "{page}"'))
def setPage(browser, page):
    browser.setCurrPage(page)

@when(parsers.parse('Перейти на эту страницу'))
def pageLink(browser):
    browser.loadPage()

@when(parsers.parse('Выбор самовывоз нажав кнопку "{btName}"'))
@when(parsers.parse('Закрыть окно "{btName}"'))
@when(parsers.parse('Нажать на кнопку "{btName}"'))
@when(parsers.parse('Выбрать раздел каталога "{btName}"'))
def click(browser, btName):
    browser.clicktButton(btName)

@when(parsers.parse('В поле для ввода "{field}" ввести "{sendKey}"'))
def sendKeysToField(browser, sendKey, field):
    browser.sendKeysToField(sendKey, field)

@when(parsers.parse('В списке "{catName}" выбрать пункт "{itemMenu}"'))
@when(parsers.parse('В области "{catName}" выбрать пункт "{itemMenu}"'))
def selectOption(browser, catName, itemMenu):
    browser.selectOption(catName, itemMenu)

@when(parsers.parse('В всплывающем окне "{popupList}" выбрать пункт "{popupItem}"'))
@when(parsers.parse('В прокручиваемом списке "{popupList}" выбрать пункт "{popupItem}"'))
@when(parsers.parse('В каталоге спуститься "{popupList}" выбрать пункт "{popupItem}"'))
@when(parsers.parse('На странице "{popupList}" выбрать пункт "{popupItem}"'))
def selectItemOption(browser, popupList, popupItem):
    browser.selectItemOption(popupList, popupItem)

@when(parsers.parse('На общей странице нажать на "{itemName}"'))
@when(parsers.parse('Перейти в "{itemName}"'))
@when(parsers.parse('Добавить "{itemName}"'))
@when(parsers.parse('Нажать на "{itemName}"'))
@when(parsers.parse('Нажать на "<btnCatalog>"'))
def loadItemPage(browser, itemName):
    browser.loadItemPage(itemName)

@then(parsers.parse('Закрыть окно'))
def closeWin(browser):
    browser.closeWin()


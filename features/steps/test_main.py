import pytest_bdd
from pytest_bdd import scenario, when, then
from pySelen.pytestSelen import *
#from functools import partial
#from pySelen.testMachine import TestMachine as TM

#scenario = partial(pytest_bdd.scenario, '..\\mainPage.feature')


# main Page

@scenario('..\\mainPage.feature', 'test 1')
def test_1(browser):
    pass

# popupWin

@scenario('..\\popupWin.feature', 'test 2')
def test_2(browser):
    pass

@scenario('..\\popupWin.feature', 'test 3')
def test_3(browser):
    pass

@scenario('..\\popupWin.feature', 'test 4')
def test_4(browser):
    pass

# our Magazine

@scenario('..\\ourMagazine.feature', 'test 5')
def test_5(browser):
    pass

@scenario('..\\ourMagazine.feature', 'test 6')
def test_6(browser):
    pass

# feedBack

@scenario('..\\feedBack.feature', 'test 7')
def test_7(browser):
    pass

# catalog

@scenario('..\\catalog.feature', 'test 8')
def test_8(browser):
    pass

@scenario('..\\catalog.feature', 'test 9')
def test_9(browser):
    pass

@scenario('..\\catalog.feature', 'test 10')
def test_10(browser):
    pass

@scenario('..\\catalog.feature', 'test 11')
def test_11(browser):
    pass

# regiostrationEntrance

@scenario('..\\regEntr.feature', 'test 12')
def test_12(browser):
    pass

# basket

@scenario('..\\basket.feature', 'test 13')
def test_13(browser):
    pass

@scenario('..\\basket.feature', 'test 14')
def test_14(browser):
    pass

@scenario('..\\basket.feature', 'test 15')
def test_15(browser):
    pass

